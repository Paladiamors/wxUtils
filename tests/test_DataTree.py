'''
Created on Jan 2, 2020

@author: justin
'''
import unittest
from data import DataTree
from pprint import pprint


class Test(unittest.TestCase):

    def test_AddFolder(self):
        dataTree = DataTree()
        dataTree.AddFolder("/some/path")
        dataTree.AddFolder("/some/path2")

        self.assertTrue("/some/path" in dataTree.Folders)
        self.assertTrue("/some" in dataTree.Folders)

        self.assertTrue(dataTree.Folders["/some"] == ["/some/path", "/some/path2"])

    def test_AddObject(self):

        dataTree = DataTree()
        dataTree.AddObject("/some/path/syslog", {"Size": 100})

        self.assertTrue("/some/path" in dataTree.Folders)
        self.assertTrue("/some" in dataTree.Folders)

        self.assertTrue(dataTree.GetObject("/some/path/syslog") ==
                        {"Size": 100, "Type": "Object", "Path": "/some/path/syslog"})
        self.assertTrue(dataTree.GetObject("/some/path/") == {"Type": "Folder", "Path": "/some/path"})

    def test_RemoveObject(self):

        dataTree = DataTree()
        dataTree.AddFolder("/some/path")
        dataTree.AddObject("/some/path/data1")
        dataTree.AddObject("/some/path/data2")

        dataTree.RemoveObject("/some/path/data1")
        self.assertTrue("/some/path/data1" not in dataTree.Objects)
        self.assertTrue("/some/path/data1" not in dataTree.Folders["/some/path"])

    def test_RemoveFolder(self):

        dataTree = DataTree()
        dataTree.AddFolder("/some/path")
        dataTree.AddFolder("/some/path/OtherFolder")
        dataTree.AddObject("/some/path/data1")
        dataTree.AddObject("/some/path/data2")
        dataTree.AddObject("/some/path/data3")
        dataTree.AddObject("/some/path/OtherFolder/data1")
        dataTree.RemoveFolder("/some/path")
        
        self.assertTrue("/some" in dataTree.Objects)
        self.assertTrue("/" in dataTree.Objects)
        self.assertTrue("/some" in dataTree.Folders)
        self.assertTrue("/" in dataTree.Folders)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
