'''
Created on Jan 2, 2020

@author: justin
'''
import unittest
import wx
from TreeListCtrl import TreeListCtrl
from pprint import pprint


class MainFrame(wx.Frame):

    def __init__(self, *args, **kw):
        wx.Frame.__init__(self, *args, **kw)
        self.Panel = MainPanel(self)


class MainPanel(wx.Panel):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, -1, style=wx.WANTS_CHARS)
        self.Control = TreeListCtrl(self)


class Test(unittest.TestCase):

    def setUp(self):
        self.app = wx.App()
        self.MainFrame = MainFrame(None, title="test")
        self.Control = self.MainFrame.Panel.Control

    def tearDown(self):

        del(self.MainFrame)
        del(self.Control)
        del(self.app)

    def test_check_root(self):

        # check that the root object is there
        objectdata = self.Control.GetObject("/")
        self.assertTrue(objectdata.get("Item") is not None)
        self.assertTrue(objectdata["Type"] == "Folder")

    def test_add_folder(self):
        
        self.Control.AddFolder("/this/is/some/folder/")
        pprint(self.Control.DataTree.Folders)
        pprint(self.Control.DataTree.Objects)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
