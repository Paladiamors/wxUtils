'''
Created on Jan 3, 2020

@author: justin
'''

import os
import wx
from .data import DataTree


class MenuBar(wx.MenuBar):

    def __init__(self, Frame):
        wx.MenuBar.__init__(self, 0)
        self.Frame = Frame
        self.Frame.SetMenuBar(self)
        self.DataTree = DataTree()
        self.DataTree.AddObject("/", {"Item": self})

    def AddSubMenu(self, path, altname = None):

        path = self.DataTree.FormatPath(path)
        if path in self.DataTree.Folders:
            return self.DataTree.GetObject(path)["Item"]

        parent, name = os.path.split(path)
        if altname:
            name = altname
            
        if parent not in self.DataTree.Folders:
            parentitem = self.AddSubMenu(parent)
        else:
            parentitem = self.DataTree.GetObject(parent)["Item"]

        if parent == "/":
            submenu = wx.Menu()
            self.Append(submenu, name)
            self.DataTree.AddFolder(path, {"Item": submenu})

        else:
            submenu = wx.Menu()
            parentitem.AppendSubMenu(submenu, name)
            self.DataTree.AddFolder(path, {"Item": submenu})
            
        return submenu

    def AddMenu(self, path, altname = None):

        path = self.DataTree.FormatPath(path)
        parent, name = os.path.split(path)
        if altname:
            name = altname

        if path in self.DataTree.Objects:
            return

        if parent not in self.DataTree.Folders:
            parentitem = self.AddSubMenu(parent)
        else:
            parentitem = self.DataTree.GetObject(parent)["Item"]
        
        menuitem = wx.MenuItem(parentitem, wx.ID_ANY, name, wx.EmptyString, wx.ITEM_NORMAL)
        self.DataTree.AddObject(path, {"Item": menuitem})
        parentitem.Append(menuitem)
        
    def AddSeparator(self, path):
        """
        adds a separator into the menu
        """
        path = self.DataTree.FormatPath(path)
        submenu = self.DataTree.GetObject(path)["Item"]
        submenu.AppendSeparator()
        
    def BindMenu(self, path, func):
        """
        path = path to the menu
        func = event handler for the menu
        """
        
        data = self.DataTree.GetObject(path)
        menu = data["Item"]
        self.Frame.Bind(wx.EVT_MENU, func, id = menu.GetId())
        

