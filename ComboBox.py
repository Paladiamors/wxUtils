'''
Created on Jan 5, 2020

@author: justin
'''

import wx


class ComboBox(wx.ComboBox):
    """
    Adds some additional functionality
    """
    def __init__(self, parent, choices=[]):

        wx.ComboBox.__init__(self, parent, -1, "", wx.DefaultPosition, wx.DefaultSize, choices, 0)

        self.Choices = choices

    def AddChoice(self, choice):
        """
        Adds a choice
        """

        self.Choices.append(choice)
        self.Append(choice)

    def DeleteChoice(self, choice):
        """
        Removes a Choice
        """

        try:
            index = self.Choices.index(choice)
            self.Choices.pop(index)
            self.Delete(index)
        except ValueError:
            pass

    def SetChoice(self, choice):
        """
        Sets the selection to a choice
        """
        
        self.SetSelection(self.Choices.index(choice))
        
    def GetValue(self):
        """
        Returns the current selection
        """

        selection = self.GetSelection()
        if selection == wx.NOT_FOUND:
            return None
        else:
            return self.Choices[selection]
