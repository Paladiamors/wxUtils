'''
Created on Jan 2, 2020

@author: justin
'''

import os


class DataTree:
    """
    a tree object that handles 
    data broken down in to slashes

    we make the presumption that all information is stored as the form
    /path/to/some/data.ext
    the leading slash can be arbitrary
    the objective will be to store associated data in the data tree
    and easily access the data, we should be also able to get the contents of some Folder

    """

    def __init__(self, HeadSlash=True, TailSlash=False):
        """
        store_HeadSlash = ensures that all paths start with a slash
        store_tail_shash = ensures that all Folders have a trailng
        """

        self.HeadSlash = HeadSlash
        self.TailSlash = TailSlash
        self.Folders = {"/": []}  # stores a list of children in the Folder
        self.Objects = {"/": {"Type": "Folder"}}

    def FormatPath(self, path):
        """
        all Folders will start with a head slash
        and have no end slash to make working with os.path easier
        """
        if path == "/":
            return path
        path = "/" + path if not path.startswith("/") else path
        path = path.rstrip("/")

        return path

    def AddFolderChild(self, path):
        """
        adds a child path to the Folder
        """

        parent, _ = os.path.split(path)
        self.AddFolder(parent)
        if path not in self.Folders[parent]:
            self.Folders[parent].append(path)

    def AddFolder(self, path, data={}):
        """
        path = /some/location
        adds the Folder recursively to the Folders dictionary
        """

        if not data:
            data = {}
            
        path = self.FormatPath(path)
        data["Type"] = "Folder"
        data["Path"] = path

        if path in self.Folders:
            self.Objects[path].update(data)
            return

        # if the parent is not there, then we add the parent
        parentpath, _ = os.path.split(path)
        if parentpath not in self.Folders:
            self.AddFolder(parentpath)

        # when the folder does not exist, we just add it here
        self.Folders[path] = []
        self.Objects[path] = data
        self.AddFolderChild(path)

    def AddObject(self, path, data={}):
        """
        path = /path/to/some/File
        adds some File to the data tree
        if the path already exists the just does an update to the object
        """

        path = self.FormatPath(path)
        if path in self.Objects:
            self.Objects[path].update(data)
            return

        Folder, _ = os.path.split(path)
        
        if not data:
            # need to change the dict since
            # it gets reused 
            data = {}
            
        data["Type"] = "Object"
        data["Path"] = path
        self.Objects[path] = data
        self.AddFolder(Folder)
        self.AddFolderChild(path)

    def GetObject(self, path):
        """
        returns the object as defined by the path
        """
        path = self.FormatPath(path)
        return self.Objects.get(path, {})

    def ObjectExists(self, path):
        """
        returns true if the objects exists
        """

        path = self.FormatPath(path)
        return True if path in self.Objects else False

    def GetChildren(self, path):
        """
        path = a path to some Folder
        returns a list of paths 
        """

        path = self.FormatPath(path)
        return self.Folders.get(path, [])

    def RemovePathFromParent(self, path):
        """
        removes the object from the child list
        """
        
        path = self.FormatPath(path)
        parent, _ = os.path.split(path)
        
        childlist = self.Folders.get(parent,[])
        childlist.pop(childlist.index(path))
        
    def RemoveObject(self, path, handler = None):
        """
        path to some object, we remove the object
        handler = some handler to call when an object is removed
        """

        path = self.FormatPath(path)
        if path not in self.Objects:
            # warning should not remove this object
            return
        
        self.RemovePathFromParent(path)
        obj = self.Objects.pop(path)
        
        # if the handler is defined then process the object being removed
        if handler:
            handler(**obj)
        
    def RemoveFolder(self, path, handler = None):
        """
        Removes a folder and deletes all children
        of the folder
        """
        
        path = self.FormatPath(path)
        children = self.GetChildren(path)
        folders = []
        objects = []
        
        # group folders and objects
        for child in children:
            data = self.GetObject(child)
            if data["Type"] == "Folder":
                folders.append(data)
            else:
                objects.append(data)
           
        for obj in objects:
            self.RemoveObject(obj["Path"], handler)
        
        for folder in folders:
            self.RemoveFolder(folder["Path"], handler)
        
        # once all other things are remove
        # remove the folder
        self.Folders.pop(path)
        self.RemoveObject(path, handler)
        
