'''
Created on Jan 2, 2020

@author: justin
'''

from wx import dataview as dv
from .data import DataTree
import traceback
import wx
import os


class TreeListComparator(dv.TreeListItemComparator):

    def __init__(self):
        dv.TreeListItemComparator.__init__(self)

    def FileSort(self, treelist, first, second):

        try:
            firstpath = treelist.GetItemData(first)
            firsttype = treelist.DataTree.GetObject(firstpath)["Type"]
            secondpath = treelist.GetItemData(second)
            secondtype = treelist.DataTree.GetObject(secondpath)["Type"]
        except AttributeError:
            print("something strange")

        if firsttype == "Folder" and secondtype == "Object":
            return -1
        elif secondtype == "Folder" and firsttype == "Object":
            return 1
        else:
            if firstpath > secondpath:
                return 1
            elif firstpath < secondpath:
                return -1
            else:
                return 0

    def Compare(self, treelist, column, first, second):
        
        if column == 0:
            return self.FileSort(treelist, first, second)
        else:
            if first > second:
                return 1
            elif first < second:
                return -1
            else:
                return 0


class TreeListCtrl(dv.TreeListCtrl):

    def __init__(self, parent, pos=wx.DefaultPosition, size=wx.DefaultSize, style=dv.TL_DEFAULT_STYLE):
        """
        parent = parent panel to affix the Control to
        """
        tID = wx.NewIdRef()
        dv.TreeListCtrl.__init__(self, parent, tID.Value, pos, size, style)
        self.Bind(wx.EVT_UPDATE_UI, self.OnSize)
        self.DataTree = DataTree()
        self.isz = (16, 16)
        self.il = wx.ImageList(self.isz[0], self.isz[1])
        self.fldridx = self.il.Add(wx.ArtProvider.GetBitmap(wx.ART_FOLDER,      wx.ART_OTHER, self.isz))
        self.fldropenidx = self.il.Add(wx.ArtProvider.GetBitmap(wx.ART_FOLDER_OPEN, wx.ART_OTHER, self.isz))
        self.fileidx = self.il.Add(wx.ArtProvider.GetBitmap(wx.ART_NORMAL_FILE, wx.ART_OTHER, self.isz))
        self.SetImageList(self.il)

        # setup the root item
        self.root = self.GetRootItem()
        self.AddObject("/", {"Item": self.root})
        self.SetItemData(self.root, None)
        self.SetItemImage(self.root, self.fldridx, wx.TreeItemIcon_Normal)
        self.SetItemImage(self.root, self.fldropenidx, wx.TreeItemIcon_Expanded)

        self.Comparator = TreeListComparator()
        self.SetItemComparator(self.Comparator)

    def GetFolderItem(self, path):
        """
        path to get the folder item
        returns an item or None if the item does not exist
        """

        if path in self.DataTree.Folders:
            return self.DataTree.GetObject(path).get("Item")
        return None

    def GetObject(self, path):
        """
        path = to get the object
        """
        if path in self.DataTree.Objects:
            return self.DataTree.GetObject(path)
        return None

    def GetObjectItem(self, path):
        """
        path = to get the object item
        returns None if the item does not exist
        """

        if path in self.DataTree.Objects:
            return self.DataTree.GetObject(path).get("Item")
        return None

    def AddFolder(self, path, data = {}, colhandler = None):
        """
        path = path to add the folder
        data = additional data to add to the folder object
        colhandler = function to add additional data to the column

        if the folder item exists then return it otherwise
        creates the folder item and creates it
        """

        if not data:
            data = {}

        path = self.DataTree.FormatPath(path)
        folderitem = self.GetFolderItem(path)
        if folderitem:
            folderdata = self.GetObject(path)
            folderdata.update(data)
            if colhandler:
                colhandler(folderitem, **folderdata)
                
            return folderitem

        parentpath, name = os.path.split(path)
        parentitem = self.GetFolderItem(parentpath)
        if not parentitem:
            self.DataTree.AddFolder(parentpath)
            parentitem = self.AddFolder(parentpath)
        folderitem = self.AppendItem(parentitem, name)
        self.SetItemData(folderitem, path)
        self.SetItemImage(folderitem, self.fldridx, self.fldropenidx)
        self.DataTree.AddFolder(path, {"Item": folderitem})

        if colhandler:
            folderdata = self.GetObject(path)
            colhandler(folderitem, **folderdata)
            
        return folderitem

    def AddObject(self, path, data={}, colhandler = None):
        """
        Adds an object 
        path = path of the object
        data = data for the object
        if the object already exists the just performs an update on 
        the object dict
        colhandler = a handler to add additional to the columns
        """

        path = self.DataTree.FormatPath(path)

        objectdata = self.GetObject(path)
        if objectdata is not None:
            objectdata.update(data)
            if colhandler:
                colhandler(objectdata["Item"], **objectdata)
            return

        parentpath, name = os.path.split(path)
        parentitem = self.GetFolderItem(parentpath)
        if not parentitem:
            self.DataTree.AddFolder(parentpath)
            parentitem = self.AddFolder(parentpath)

        ObjectItem = self.AppendItem(parentitem, name)
        data["Item"] = ObjectItem
        self.SetItemData(ObjectItem, path)
        self.SetItemImage(ObjectItem, self.fileidx)
        self.DataTree.AddObject(path, data)
        
        if colhandler:
            colhandler(ObjectItem, **data)

    def RemoveItem(self, **kwargs):
        """
        wrapper for delete Item
        kwargs dict should have the item object in the
        Item key works for removal
        This function can be overloaded to do more than just to remove an item from the interface
        The delete item should be called last since other things a can fail along the way
        """

        self.DeleteItem(kwargs["Item"])

    def RemoveObject(self, path):
        """
        Given a path removes the object
        """
        self.DataTree.RemoveObject(path, self.RemoveItem)

    def RemoveFolder(self, path):
        """
        Given a path removes the folder and 
        all underlying objects
        """

        self.DataTree.RemoveFolder(path, self.RemoveItem)

    def OnSize(self, event):
        """
        This actually fixes the width problem
        """
        width = self.GetColumnWidth(0)
        if width:
            self.SetColumnWidth(0, width)


class TreeListCtrlPanelMixin:
    """
    need to see if this works...
    """

    def OnSize(self, controlname):
        def Sizer(self, event):
            w, h = self.GetClientSize()
            path_width = getattr(self, controlname).GetColumnWidth(0)
            size_width = getattr(self, controlname).GetColumnWidth(1)

            getattr(self, controlname).SetSize(0, 0, w, h)
            if path_width:
                getattr(self, controlname).SetColumnWidth(0, path_width)
            if size_width:
                getattr(self, controlname).SetColumnWidth(1, size_width)

        return Sizer
