'''
Created on Jan 5, 2020

@author: justin
'''

import wx

class Dialog(wx.Dialog):
    
    def __init__(self, parent, title, size=wx.DefaultSize, pos=wx.DefaultPosition,
                 style = wx.DEFAULT_DIALOG_STYLE, name="Dialog"):
        
        wx.Dialog.__init__(self)
        self.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        self.Create(parent, -1, title, pos, size, style, name)
        self.Sizer = wx.BoxSizer(wx.VERTICAL)
        