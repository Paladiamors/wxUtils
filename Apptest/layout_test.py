'''
Created on Jan 2, 2020

@author: justin
'''
# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.9.0 Dec 28 2019)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

###########################################################################
## Python code generated with wxFormBuilder (version 3.9.0 Dec 28 2019)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.dataview

###########################################################################
## Class MyFrame1
###########################################################################

class MyFrame1 ( wx.Frame ):

    def __init__( self, parent ):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        bSizer2 = wx.BoxSizer( wx.VERTICAL )

        self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer3 = wx.BoxSizer( wx.VERTICAL )

        self.m_dataViewCtrl1 = wx.dataview.DataViewCtrl( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_dataViewCtrl1.AppendColumn(wx.dataview.DataViewColumn())
        self.m_dataViewCtrl1.AppendColumn("Second", 100)
        self.m_dataViewCtrl1.AppendColumn("Third", 100)
        bSizer3.Add( self.m_dataViewCtrl1, 0, wx.ALL, 5 )

        self.m_dataViewCtrl2 = wx.dataview.DataViewCtrl( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_dataViewCtrl2.AppendColumn("First", 100)
        self.m_dataViewCtrl2.AppendColumn("Second", 100)
        self.m_dataViewCtrl2.AppendColumn("Third", 100)
        bSizer3.Add( self.m_dataViewCtrl2, 0, wx.ALL, 5 )


        self.m_panel1.SetSizer( bSizer3 )
        self.m_panel1.Layout()
        bSizer3.Fit( self.m_panel1 )
        bSizer2.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( bSizer2 )
        self.Layout()

        self.Centre( wx.BOTH )

        # Connect Events
        self.m_panel1.Bind( wx.EVT_SIZE, self.wtf )
        self.m_panel1.Bind( wx.EVT_UPDATE_UI, self.wtf2 )

    def __del__( self ):
        pass


    # Virtual event handlers, overide them in your derived class
    def wtf( self, event ):
        event.Skip()

    def wtf2( self, event ):
        event.Skip()




app = wx.App()
frm = MyFrame1(None)
frm.Show()
app.MainLoop()    
