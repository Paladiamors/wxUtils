'''
Created on Jan 4, 2020

@author: justin
'''

import wx
from BrowserCtrl import BrowserCtrl
from Menubar import MenuBar
from InputTable import InputTable

class MainFrame(wx.Frame):

    def __init__(self, *args, **kw):
        wx.Frame.__init__(self, *args, **kw)
        self.FrameSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.FrameSizer)

        self.MainPanel = MainPanel(self)
        self.FrameSizer.Add(self.MainPanel, 0, wx.EXPAND | wx.ALL)

        self.Menu = MenuBar(self)
        self.Menu.AddSubMenu("/File", "&File")
        self.Menu.AddMenu("/File/New", "&New\tCtrl+N")
        self.Menu.AddMenu("/File/Open", "&Open\tCtrl+O")
        self.Menu.AddMenu("/File/Save", "&Save\tCtrl+S")
        self.Menu.AddMenu("/File/Exit", "E&xit")
        self.Menu.AddMenu("/Edit/Copy")
        self.Menu.AddMenu("/Edit/Paste")

        self.Menu.BindMenu("/File/New", self.OnMenu)
        self.Menu.BindMenu("/File/Open", self.OnMenu)
        self.Menu.BindMenu("/File/Save", self.OnMenu)
        self.Menu.BindMenu("/File/Exit", self.OnMenu)

    def OnMenu(self, event):

        print("menu selected")


class MainPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, style=wx.WANTS_CHARS)

        self.ControlDefn = [{"Title": "Profile", "Type": "Choice", "Choices": []},
                            {"Title": "Main Key", "Type": "String"},
                            {"Title": "Secret Key", "Type": "String"},
                            {"Title": "Users", "Type": "Int", "Min": -100, "Max": 200, "Default": 20},
                            ]
        
        # setup of the panel sizer
        self.PanelSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.PanelSizer)
        self.Layout()
        self.PanelSizer.Fit(self)
        
        self.ProfileData = {}

        self.InputPanel = InputTable(parent, self.ControlDefn)
        self.PanelSizer.Add(self.InputPanel, 0, wx.EXPAND | wx.ALL)

        self.ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.PanelSizer.Add(self.ButtonSizer, 0, wx.ALL, 5)
        
        self.AddProfileButton = wx.Button(self, -1, "Add Profile")
        self.AddProfileButton.Bind(wx.EVT_BUTTON, self.OnAddProfileButton)
        self.ButtonSizer.Add(self.AddProfileButton, 0, wx.ALL, 5)
        
        self.DeleteProfileButton = wx.Button(self, -1, "Delete Profile")
        self.DeleteProfileButton.Bind(wx.EVT_BUTTON, self.OnDeleteProfileButton)
        self.ButtonSizer.Add(self.DeleteProfileButton, 0, wx.ALL, 5)
        
        self.SaveProfilesButton = wx.Button(self, -1, "SaveProfiles")
        self.SaveProfilesButton.Bind(wx.EVT_BUTTON, self.OnSaveProfilesButton)
        self.ButtonSizer.Add(self.SaveProfilesButton, 0, wx.ALL, 5)
        
        self.ChoiceCtrl = self.InputPanel.GetControl("Profile")
        self.ChoiceCtrl.Bind(wx.EVT_COMBOBOX, self.OnChangeChoice)
        # Add your controls below here and start adding them to the sizer
        
    def OnAddProfileButton(self, event):
        dlg = Dialog(self, "Add Data")
        dlg.CenterOnScreen()
        
        val = dlg.ShowModal()
        
        if val == wx.ID_OK:
            print("the ok button was pressed")
            print("Processing profile")
            data = dlg.InputTable.GetData()
            profile = data.pop("Profile")
            self.ProfileData[profile] = data
            
            ChoiceCtrl = self.InputPanel.GetControl("Profile")
            ChoiceCtrl.AddChoice(profile)
            ChoiceCtrl.SetChoice(profile)
            self.InputPanel.SetData(data)
            
    def OnDeleteProfileButton(self, event):
        
        print("deleting profile")
        profile = self.ChoiceCtrl.GetValue()
        if profile:
            index = self.ChoiceCtrl.GetSelection()
            self.ChoiceCtrl.DeleteChoice(profile)
            self.ProfileData.pop(profile)
            if index > 0:
                index = index - 1
                self.ChoiceCtrl.SetSelection(index)
                profile = self.ChoiceCtrl.GetValue()
                data = self.ProfileData[profile]
                self.InputPanel.SetData(data)
        
            
    def OnSaveProfilesButton(self, event):
        pass
    
    def OnChangeChoice(self, event):
        print("choice changed")
        profile = self.ChoiceCtrl.GetValue()
        data = self.ProfileData[profile].copy()
        self.InputPanel.SetData(data)
        
class Dialog(wx.Dialog):
    
    def __init__(self, parent, title, size=wx.DefaultSize, pos=wx.DefaultPosition,
                 style = wx.DEFAULT_DIALOG_STYLE, name="Dialog"):
        
        wx.Dialog.__init__(self)
        self.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        self.Create(parent, -1, title, pos, size, style, name)
        self.Sizer = wx.BoxSizer(wx.VERTICAL)
        
        self.ControlDefn = [{"Title": "Profile", "Type": "String"},
                            {"Title": "Main Key", "Type": "String"},
                            {"Title": "Secret Key", "Type": "String"},
                            {"Title": "Users", "Type": "Int", "Min": -100, "Max": 200, "Default": 20},
                            ]
        
        self.InputTable = InputTable(self, self.ControlDefn)
        self.Sizer.Add(self.InputTable, 0, wx.ALL, 5)

        self.ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.Sizer.Add(self.ButtonSizer, 0, wx.ALL, 5)
        
        self.OkButton = wx.Button(self, wx.ID_OK, "OK")
        self.ButtonSizer.Add(self.OkButton, 0, wx.ALL, 5)
        
        self.CancelButton = wx.Button(self, wx.ID_CANCEL, "Cancel")
        self.ButtonSizer.Add(self.CancelButton, 0, wx.ALL, 5)
        

if __name__ == "__main__":
    app = wx.App()
    frm = MainFrame(None, size=(500,500), title='Browser')
    frm.Show()
    app.MainLoop()
