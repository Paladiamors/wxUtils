'''
Created on Jan 2, 2020

@author: justin
'''

import wx
from wx import dataview as dv
import os
from TreeListCtrl import TreeListCtrl
from BrowserCtrl import BrowserCtrl
from pprint import pprint


class MainFrame(wx.Frame):

    def __init__(self, *args, **kw):
        wx.Frame.__init__(self, *args, **kw)
        self.FrameSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.FrameSizer)

        self.MainPanel = MainPanel(self)
        self.FrameSizer.Add(self.MainPanel, 1, wx.EXPAND|wx.ALL)

class MainPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, style=wx.WANTS_CHARS)

        # setup of the panel sizer        
        self.PanelSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.PanelSizer)
        self.Layout()
        self.PanelSizer.Fit(self)
        
        self.LeftControl = BrowserCtrl(self)
        
        self.RightControl = TreeListCtrl(self)
        self.RightControl.AppendColumn("File", width = 100, flags=dv.DATAVIEW_COL_RESIZABLE|dv.DATAVIEW_COL_SORTABLE)
        self.RightControl.AppendColumn("Size", width = 100)
        self.RightControl.AppendColumn("Modified Date", width = 100)
        
        self.PanelSizer.Add(self.LeftControl, 1, wx.EXPAND | wx.ALL)
        self.PanelSizer.Add(self.RightControl, 1, wx.EXPAND | wx.ALL)
        
    def Sort(self, event):
        
        self.LeftControl.SetSortColumn(0)

if __name__ == "__main__":
    app = wx.App()
    frm = MainFrame(None, title='Browser')
    frm.Show()
    app.MainLoop()

