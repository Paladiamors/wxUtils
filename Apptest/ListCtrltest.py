'''
Created on Jan 4, 2020

@author: justin
'''

import wx
import random
from BrowserCtrl import BrowserCtrl
from Menubar import MenuBar
from ListCtrl import ListCtrl
from PopupMenu import PopupMenu


class MainFrame(wx.Frame):

    def __init__(self, *args, **kw):
        wx.Frame.__init__(self, *args, **kw)
        self.FrameSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.FrameSizer)

        self.MainPanel = MainPanel(self)
        self.FrameSizer.Add(self.MainPanel, 1, wx.EXPAND | wx.ALL)

        self.Menu = MenuBar(self)
        self.Menu.AddSubMenu("/File", "&File")
        self.Menu.AddMenu("/File/New", "&New\tCtrl+N")
        self.Menu.AddMenu("/File/Open", "&Open\tCtrl+O")
        self.Menu.AddMenu("/File/Save", "&Save\tCtrl+S")
        self.Menu.AddMenu("/File/Exit", "E&xit")
        self.Menu.AddMenu("/Edit/Copy")
        self.Menu.AddMenu("/Edit/Paste")

        self.Menu.BindMenu("/File/New", self.OnMenu)
        self.Menu.BindMenu("/File/Open", self.OnMenu)
        self.Menu.BindMenu("/File/Save", self.OnMenu)
        self.Menu.BindMenu("/File/Exit", self.OnMenu)

    def OnMenu(self, event):

        print("menu selected")


class MainPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, style=wx.WANTS_CHARS)

        # setup of the panel sizer
        self.PanelSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.PanelSizer)
        self.Layout()
        self.PanelSizer.Fit(self)

        self.LeftControl = BrowserCtrl(self)
        self.ListCtrl = ListCtrl(self, -1)
        
        self.PanelSizer.Add(self.ListCtrl, 1, wx.EXPAND | wx.ALL)
        
        Columns, Data = self.CreateData()
        self.ListCtrl.SetColumns(Columns)
        self.ListCtrl.SetData(Data)
        self.ListCtrl.PopulateData()
        
        self.PopupMenuSelections = [{"Title": "Delete All", "Func": self.DeleteAll},
                                    {"Title": "Repopulate", "Func": self.Repopulate}]
        
        self.ListCtrl.Bind(wx.EVT_COMMAND_RIGHT_CLICK, self.OnRightClick)
        self.ListCtrl.Bind(wx.EVT_RIGHT_UP, self.OnRightClick)
        
    def CreateData(self):
        
        names = ["Alice", "Bob", "Jim", "Katherine"]
        
        Data = [{"Name": random.choice(names), "Age": random.randint(1,101), "IQ": random.randint(70, 131)}
                for x in range(20)]
        
        Columns = ["Name", "Age", "IQ"]
        return Columns, Data 

    def OnRightClick(self, event):
        popupmenu = PopupMenu(self.PopupMenuSelections)
        self.PopupMenu(popupmenu)
        popupmenu.Destroy()
    
    def DeleteAll(self, event):
        self.ListCtrl.DeleteAllItems()
    
    def Repopulate(self, event):
        self.ListCtrl.PopulateData()
        
        


if __name__ == "__main__":
    app = wx.App()
    frm = MainFrame(None, title='ListCtrl')
    frm.Show()
    app.MainLoop()

