'''
Created on Jan 4, 2020

@author: justin
'''

# -*- coding: utf-8 -*-

###########################################################################
# Python code generated with wxFormBuilder (version 3.9.0 Dec 28 2019)
# http://www.wxformbuilder.org/
##
# PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
from wxPrim.Menubar import MenuBar

###########################################################################
# Class MyFrame1
###########################################################################


class MainFrame(wx.Frame):

    def __init__(self, *args, **kw):
        wx.Frame.__init__(self, *args, **kw)
        self.FrameSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.FrameSizer)

        self.MainPanel = MainPanel(self)
        self.FrameSizer.Add(self.MainPanel, 1, wx.EXPAND | wx.ALL)

        self.Menu = MenuBar(self)
        self.Menu.AddSubMenu("/File", "&File")
        self.Menu.AddMenu("/File/New", "&New\tCtrl+N")
        self.Menu.AddMenu("/File/Open", "&Open\tCtrl+O")
        self.Menu.AddMenu("/File/Save", "&Save\tCtrl+S")
        self.Menu.AddMenu("/File/Exit", "E&xit")
        self.Menu.AddMenu("/Edit/Copy")
        self.Menu.AddMenu("/Edit/Paste")

        self.Menu.BindMenu("/File/New", self.OnMenu)
        self.Menu.BindMenu("/File/Open", self.OnMenu)
        self.Menu.BindMenu("/File/Save", self.OnMenu)
        self.Menu.BindMenu("/File/Exit", self.OnMenu)

        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        bSizer1 = wx.BoxSizer( wx.VERTICAL )

        self.m_panel3 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        fgSizer1 = wx.FlexGridSizer( 0, 2, 0, 0 )
        fgSizer1.AddGrowableCol( 1 )
        fgSizer1.SetFlexibleDirection( wx.BOTH )
        fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_staticText3 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Some longer Label", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )

        fgSizer1.Add( self.m_staticText3, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

        self.m_textCtrl3 = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( self.m_textCtrl3, 1, wx.ALL|wx.EXPAND, 5 )

        self.m_staticText5 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"A Shorter Label", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText5.Wrap( -1 )

        fgSizer1.Add( self.m_staticText5, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

        self.m_textCtrl4 = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( self.m_textCtrl4, 1, wx.ALL|wx.EXPAND, 5 )


        self.m_panel3.SetSizer( fgSizer1 )
        self.m_panel3.Layout()
        fgSizer1.Fit( self.m_panel3 )
        bSizer1.Add( self.m_panel3, 0, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( bSizer1 )
        self.Layout()
        
    def OnMenu(self, event):

        print("menu selected")


class MainPanel(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, style=wx.WANTS_CHARS)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        self.m_panel3 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer1 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer1.SetFlexibleDirection(wx.BOTH)
        fgSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText3 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"Some longer Label",
                                           wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)

        fgSizer1.Add(self.m_staticText3, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

        self.m_textCtrl3 = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer1.Add(self.m_textCtrl3, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText5 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"A Shorter Label",
                                           wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText5.Wrap(-1)

        fgSizer1.Add(self.m_staticText5, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

        self.m_textCtrl4 = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer1.Add(self.m_textCtrl4, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel3.SetSizer(fgSizer1)
        self.m_panel3.Layout()
        fgSizer1.Fit(self.m_panel3)
        bSizer1.Add(self.m_panel3, 0, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(bSizer1)
        self.Layout()

        self.Centre(wx.BOTH)

    def __del__(self):
        pass


if __name__ == "__main__":
    app = wx.App()
    frm = MainFrame(None, title='Browser')
    frm.Show()
    app.MainLoop()
