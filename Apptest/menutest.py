'''
Created on Jan 3, 2020

@author: justin
'''

import wx
from wx import dataview as dv
import os
from pprint import pprint
from BrowserCtrl import BrowserCtrl
from Menubar import MenuBar


class MainFrame(wx.Frame):

    def __init__(self, *args, **kw):
        wx.Frame.__init__(self, *args, **kw)
        self.FrameSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.FrameSizer)

        self.MainPanel = MainPanel(self)
        self.FrameSizer.Add(self.MainPanel, 1, wx.EXPAND | wx.ALL)

        self.Menu = MenuBar(self)
        self.Menu.AddSubMenu("/File", "&File")
        self.Menu.AddMenu("/File/New", "&New")
        self.Menu.AddMenu("/File/Open", "&Open")
        self.Menu.AddMenu("/File/Save", "&Save")
        self.Menu.AddSeparator("/File")
        self.Menu.AddMenu("/File/Exit", "E&xit\tCtrl+X")
        
        self.Menu.AddMenu("/Edit/Copy")
        self.Menu.AddMenu("/Edit/Paste")
        self.Menu.AddSeparator("/Edit")
        self.Menu.AddMenu("/Edit/Font/Times")
        self.Menu.AddMenu("/Edit/Font/New")
        self.Menu.AddMenu("/Edit/Font/Roman")

        self.Menu.BindMenu("/File/New", self.OnMenu)
        self.Menu.BindMenu("/File/Open", self.OnMenu)
        self.Menu.BindMenu("/File/Save", self.OnMenu)
        self.Menu.BindMenu("/File/Exit", self.OnMenu)
       
    def OnMenu(self, event):
        
        print("menu selected")

class MainPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, style=wx.WANTS_CHARS)

        # setup of the panel sizer
        self.PanelSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.PanelSizer)
        self.Layout()
        self.PanelSizer.Fit(self)

        self.LeftControl = BrowserCtrl(self)
        self.PanelSizer.Add(self.LeftControl, 1, wx.EXPAND | wx.ALL)


if __name__ == "__main__":
    app = wx.App()
    frm = MainFrame(None, title='Browser')
    frm.Show()
    app.MainLoop()

