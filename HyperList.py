'''
Created on Jan 4, 2020

@author: justin

Learning how to use HyperTreeList
'''

#!/usr/bin/env python
#   Tags: phoenix-port, py3-port

import os
import string
import random

import wx
import wx.lib.colourselect as csel
import wx.lib.colourutils as cutils

import wx.lib.agw.hypertreelist as HTL
bitmapDir = "agw/bitmaps/"


#---------------------------------------------------------------------------
# HyperTreeList Demo Implementation
#---------------------------------------------------------------------------
class HyperTreeListDemo(wx.Frame):

    def __init__(self, parent, log):

        wx.Frame.__init__(self, parent)

        self.SetMinSize((640, 480))
        self.SetIcon(images.Mondrian.GetIcon())
        self.SetTitle("HyperTreeList wxPython Demo ;-)")

        self.CenterOnParent()

        statusbar = self.CreateStatusBar(2)
        statusbar.SetStatusWidths([-2, -1])
        # statusbar fields
        statusbar_fields = [("HyperTreeList Demo, Andrea Gavana @ 08 May 2007"),
                            ("Welcome To wxPython!")]

        for i in range(len(statusbar_fields)):
            statusbar.SetStatusText(statusbar_fields[i], i)

        self.CreateMenuBar()

        self.oldicons = 0

        splitter = wx.SplitterWindow(self, -1, style=wx.CLIP_CHILDREN | wx.SP_LIVE_UPDATE | wx.SP_3D)
        panel = wx.Panel(splitter, -1, style=wx.WANTS_CHARS)

        # Set up a log window
        self.log = log

        sizer = wx.BoxSizer(wx.VERTICAL)
        # Create the HyperTreeList
        self.tree = HyperTreeList(panel, -1, log=self.log)

        sizer.Add(self.tree, 1, wx.EXPAND)
        panel.SetSizer(sizer)
        sizer.Layout()

        self.columnBackgroundColours = [wx.LIGHT_GREY for i in range(self.tree.GetColumnCount())]


        self.leftpanel = wx.ScrolledWindow(splitter, -1, style=wx.SUNKEN_BORDER)
        self.PopulateLeftPanel(self.tree.styles, self.tree.events)

        splitter.SplitVertically(self.leftpanel, panel, 300)

        splitter.SetMinimumPaneSize(120)
        self.leftpanel.SetBackgroundColour(wx.WHITE)
        self.leftpanel.SetScrollRate(20, 20)

        self.Fit()


    def CreateMenuBar(self):

        file_menu = wx.Menu()

        AS_EXIT = wx.NewIdRef()
        file_menu.Append(AS_EXIT, "&Exit")
        self.Bind(wx.EVT_MENU, self.OnClose, id=AS_EXIT)

        help_menu = wx.Menu()

        AS_ABOUT = wx.NewIdRef()
        help_menu.Append(AS_ABOUT, "&About...")
        self.Bind(wx.EVT_MENU, self.OnAbout, id=AS_ABOUT)

        menu_bar = wx.MenuBar()

        menu_bar.Append(file_menu, "&File")
        menu_bar.Append(help_menu, "&Help")

        self.SetMenuBar(menu_bar)


    def OnClose(self, event):

        self.Destroy()


    def OnAbout(self, event):

        msg = "This Is The About Dialog Of The HyperTreeList Demo.\n\n" + \
              "Author: Andrea Gavana @ 08 May 2007\n\n" + \
              "Please Report Any Bug/Requests Of Improvements\n" + \
              "To Me At The Following Adresses:\n\n" + \
              "andrea.gavana@maerskoil.com\n" + "andrea.gavana@gmail.com\n\n" + \
              "Welcome To wxPython " + wx.VERSION_STRING + "!!"

        dlg = wx.MessageDialog(self, msg, "HyperTreeList Demo",
                               wx.OK | wx.ICON_INFORMATION)

        if wx.Platform != '__WXMAC__':
            dlg.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False))

        dlg.ShowModal()
        dlg.Destroy()


    def PopulateLeftPanel(self, styles, events):

        mainsizer = wx.BoxSizer(wx.VERTICAL)
        recreatetree = wx.Button(self.leftpanel, -1, "Recreate HyperTreeList")
        mainsizer.Add(recreatetree, 0, wx.ALL|wx.ALIGN_CENTER, 10)
        recreatetree.Bind(wx.EVT_BUTTON, self.OnRecreateTree)

        staticboxstyles = wx.StaticBox(self.leftpanel, -1, "HyperTreeList Styles")
        stylesizer = wx.StaticBoxSizer(staticboxstyles, wx.VERTICAL)
        staticboxcolumns = wx.StaticBox(self.leftpanel, -1, "HyperTreeList Columns")
        columnssizer = wx.StaticBoxSizer(staticboxcolumns, wx.VERTICAL)
        staticboxevents = wx.StaticBox(self.leftpanel, -1, "HyperTreeList Events")
        eventssizer = wx.StaticBoxSizer(staticboxevents, wx.VERTICAL)
        staticboxcolours = wx.StaticBox(self.leftpanel, -1, "HyperTreeList Images/Colours")
        colourssizer = wx.StaticBoxSizer(staticboxcolours, wx.VERTICAL)
        staticboxthemes = wx.StaticBox(self.leftpanel, -1, "HyperTreeList Themes/Gradients")
        themessizer = wx.StaticBoxSizer(staticboxthemes, wx.VERTICAL)

        self.treestyles = []
        self.treeevents = []

        for count, style in enumerate(styles):

            if count == 0:
                tags = wx.ALL
            else:
                tags = wx.LEFT|wx.RIGHT|wx.BOTTOM

            if style != "TR_DEFAULT_STYLE":
                check = wx.CheckBox(self.leftpanel, -1, style)
                stylesizer.Add(check, 0, tags, 3)

                if style in ["TR_HAS_BUTTONS", "TR_HAS_VARIABLE_ROW_HEIGHT"]:
                    check.SetValue(1)

                if style == "TR_HAS_VARIABLE_ROW_HEIGHT":
                    check.Enable(False)

                check.Bind(wx.EVT_CHECKBOX, self.OnCheckStyle)
                self.treestyles.append(check)

        flexgridcolumn = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        flexgridcolumn.AddGrowableCol(0)
        label = wx.StaticText(self.leftpanel, -1, "Text Font")
        label.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, 0, ""))
        self.buttonfont = wx.Button(self.leftpanel, -1, "Choose...")
        self.buttonfont.Bind(wx.EVT_BUTTON, self.OnColumnFont)
        flexgridcolumn.Add(label, 0, wx.ALIGN_CENTER_VERTICAL)
        flexgridcolumn.Add(self.buttonfont, 0)

        label = wx.StaticText(self.leftpanel, -1, "Text Colour")
        label.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, 0, ""))
        self.columncolour = csel.ColourSelect(self.leftpanel, -1, "Choose...", wx.BLACK)
        self.columncolour.Bind(csel.EVT_COLOURSELECT, self.OnColumnColour)
        flexgridcolumn.Add(label, 0, wx.ALIGN_CENTER_VERTICAL)
        flexgridcolumn.Add(self.columncolour, 0)

        label = wx.StaticText(self.leftpanel, -1, "Column Background Colour")
        label.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, 0, ""))
        self.columnchoice = wx.Choice(self.leftpanel, -1, choices = ["1","2","3"])
        self.columnchoice.SetSelection(0)
        self.columnchoice.Bind(wx.EVT_CHOICE, self.OnColumnChoiceChanged)
        self.columnbackgroundcolour = csel.ColourSelect(self.leftpanel, -1, "Choose...", wx.LIGHT_GREY)
        self.columnbackgroundcolour.Bind(csel.EVT_COLOURSELECT, self.OnColumnBackgroundColour)
        flexgridcolumn.Add(label, 0, wx.ALIGN_CENTER_VERTICAL)
        hSizer = wx.BoxSizer(wx.HORIZONTAL)
        hSizer.Add(self.columnchoice, 0, wx.RIGHT, 5)
        hSizer.Add(self.columnbackgroundcolour)
        flexgridcolumn.Add(hSizer)

        label = wx.StaticText(self.leftpanel, -1, "Alignment")
        label.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, 0, ""))
        alignment = wx.Choice(self.leftpanel, -1, choices=["wx.LEFT", "wx.CENTER", "wx.RIGHT"])
        alignment.Bind(wx.EVT_CHOICE, self.OnColumnAlignment)
        flexgridcolumn.Add(label, 0, wx.ALIGN_CENTER_VERTICAL)
        flexgridcolumn.Add(alignment, 0)

        label = wx.StaticText(self.leftpanel, -1, "Width Options")
        label.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, 0, ""))
        widthoptions = wx.Choice(self.leftpanel, -1, choices=["USEHEADER", "AUTOSIZE"])
        widthoptions.Bind(wx.EVT_CHOICE, self.OnColumnWidth)
        flexgridcolumn.Add(label, 0, wx.ALIGN_CENTER_VERTICAL)
        flexgridcolumn.Add(widthoptions, 0)

        columnimages = wx.CheckBox(self.leftpanel, -1, "Images On Columns", style=wx.ALIGN_RIGHT)
        columnimages.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, 0, ""))
        columnimages.Bind(wx.EVT_CHECKBOX, self.OnColumnImages)
        flexgridcolumn.Add(columnimages, 0, wx.ALIGN_CENTER_VERTICAL)
        flexgridcolumn.Add((20,20), 0)

        columnimages = wx.CheckBox(self.leftpanel, -1, "Custom Renderer", style=wx.ALIGN_RIGHT)
        columnimages.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, 0, ""))
        columnimages.Bind(wx.EVT_CHECKBOX, self.OnCustomRenderer)
        flexgridcolumn.Add(columnimages, 0, wx.ALIGN_CENTER_VERTICAL)
        flexgridcolumn.Add((20,20), 0)

        columnssizer.Add(flexgridcolumn, 1, wx.EXPAND)

        for count, event in enumerate(events):

            if count == 0:
                tags = wx.ALL
            else:
                tags = wx.LEFT|wx.RIGHT|wx.BOTTOM

            if count not in [6, 17, 22, 23]:
                check = wx.CheckBox(self.leftpanel, -1, event)
                eventssizer.Add(check, 0, tags, 3)

                if event in ["EVT_TREE_ITEM_EXPANDED", "EVT_TREE_ITEM_COLLAPSED",
                             "EVT_TREE_SEL_CHANGED", "EVT_TREE_SEL_CHANGING"]:

                    check.SetValue(1)

                check.Bind(wx.EVT_CHECKBOX, self.OnCheckEvent)
                self.treeevents.append(check)

        sizer1 = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self.leftpanel, -1, "Connection Pen")
        font = label.GetFont()
        font.SetWeight(wx.FONTWEIGHT_BOLD)
        label.SetFont(font)
        buttonconnection = wx.Button(self.leftpanel, -1, "Choose...")
        buttonconnection.Bind(wx.EVT_BUTTON, self.OnButtonConnection)
        sizer1.Add(label, 0, wx.ALL|wx.ALIGN_CENTER, 5)
        sizer1.Add((1,0), 1, wx.EXPAND)
        sizer1.Add(buttonconnection, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_RIGHT, 5)

        sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self.leftpanel, -1, "Border Pen")
        font = label.GetFont()
        font.SetWeight(wx.FONTWEIGHT_BOLD)
        label.SetFont(font)
        buttonborder = wx.Button(self.leftpanel, -1, "Choose...")
        buttonborder.Bind(wx.EVT_BUTTON, self.OnButtonBorder)
        sizer2.Add(label, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.ALIGN_CENTER, 5)
        sizer2.Add((1,0), 1, wx.EXPAND)
        sizer2.Add(buttonborder, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_RIGHT, 5)

        sizer3 = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self.leftpanel, -1, "Tree Buttons")
        font = label.GetFont()
        font.SetWeight(wx.FONTWEIGHT_BOLD)
        label.SetFont(font)
        buttontree = wx.Button(self.leftpanel, -1, "Choose...")
        buttontree.Bind(wx.EVT_BUTTON, self.OnButtonTree)
        sizer3.Add(label, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.ALIGN_CENTER, 5)
        sizer3.Add((1,0), 1, wx.EXPAND)
        sizer3.Add(buttontree, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_RIGHT, 5)

        sizer4 = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self.leftpanel, -1, "Check/Radio Buttons")
        font = label.GetFont()
        font.SetWeight(wx.FONTWEIGHT_BOLD)
        label.SetFont(font)
        buttoncr = wx.Button(self.leftpanel, -1, "Choose...")
        buttoncr.Bind(wx.EVT_BUTTON, self.OnButtonCheckRadio)
        sizer4.Add(label, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.ALIGN_CENTER, 5)
        sizer4.Add((1,0), 1, wx.EXPAND)
        sizer4.Add(buttoncr, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_RIGHT, 5)

        sizer5 = wx.BoxSizer(wx.HORIZONTAL)
        radioimage = wx.RadioButton(self.leftpanel, -1, "Image Background", style=wx.RB_GROUP)
        radioimage.Bind(wx.EVT_RADIOBUTTON, self.OnBackgroundImage)
        self.imagebutton = wx.Button(self.leftpanel, -1, "Choose...")
        self.imagebutton.Bind(wx.EVT_BUTTON, self.OnChooseImage)
        sizer5.Add(radioimage, 0, wx.ALL|wx.ALIGN_CENTER, 5)
        sizer5.Add((1,0), 1, wx.EXPAND)
        sizer5.Add(self.imagebutton, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_RIGHT, 5)

        sizer6 = wx.BoxSizer(wx.HORIZONTAL)
        radiobackground = wx.RadioButton(self.leftpanel, -1, "Background Colour")
        radiobackground.Bind(wx.EVT_RADIOBUTTON, self.OnBackgroundColour)
        self.backbutton = csel.ColourSelect(self.leftpanel, -1, "Choose...", wx.WHITE)
        self.backbutton.Bind(csel.EVT_COLOURSELECT, self.OnChooseBackground)
        sizer6.Add(radiobackground, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.ALIGN_CENTER, 5)
        sizer6.Add((1,0), 1, wx.EXPAND)
        sizer6.Add(self.backbutton, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.ALIGN_CENTER, 5)

        colourssizer.Add(sizer1, 0, wx.EXPAND)
        colourssizer.Add(sizer2, 0, wx.EXPAND)
        colourssizer.Add(sizer3, 0, wx.EXPAND)
        colourssizer.Add(sizer4, 0, wx.EXPAND)
        colourssizer.Add(sizer5, 0, wx.EXPAND)
        colourssizer.Add(sizer6, 0, wx.EXPAND)

        sizera = wx.BoxSizer(wx.HORIZONTAL)
        self.checknormal = wx.CheckBox(self.leftpanel, -1, "Standard Colours")
        self.focus = csel.ColourSelect(self.leftpanel, -1, "Focus",
                                       self.tree.GetHilightFocusColour())
        self.unfocus = csel.ColourSelect(self.leftpanel, -1, "Non-Focus",
                                         self.tree.GetHilightNonFocusColour())
        self.checknormal.Bind(wx.EVT_CHECKBOX, self.OnCheckNormal)
        self.focus.Bind(csel.EVT_COLOURSELECT, self.OnFocusColour)
        self.unfocus.Bind(csel.EVT_COLOURSELECT, self.OnNonFocusColour)
        sizera1 = wx.BoxSizer(wx.VERTICAL)
        sizera1.Add(self.focus, 0, wx.BOTTOM, 2)
        sizera1.Add(self.unfocus, 0)
        sizera.Add(self.checknormal, 0, wx.ALL, 3)
        sizera.Add((1, 0), 1, wx.EXPAND)
        sizera.Add(sizera1, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 3)

        sizerb = wx.BoxSizer(wx.VERTICAL)
        self.checkgradient = wx.CheckBox(self.leftpanel, -1, "Gradient Theme")
        self.checkgradient.Bind(wx.EVT_CHECKBOX, self.OnCheckGradient)
        sizerb1 = wx.BoxSizer(wx.HORIZONTAL)
        sizerb1.Add((10, 0))
        self.radiohorizontal = wx.RadioButton(self.leftpanel, -1, "Horizontal", style=wx.RB_GROUP)
        self.radiohorizontal.Bind(wx.EVT_RADIOBUTTON, self.OnHorizontal)
        sizerb1.Add(self.radiohorizontal, 0, wx.TOP|wx.BOTTOM, 3)
        sizerb2 = wx.BoxSizer(wx.HORIZONTAL)
        sizerb2.Add((10, 0))
        self.radiovertical = wx.RadioButton(self.leftpanel, -1, "Vertical")
        self.radiovertical.Bind(wx.EVT_RADIOBUTTON, self.OnVertical)
        sizerb2.Add(self.radiovertical, 0, wx.BOTTOM, 3)
        sizerb3 = wx.BoxSizer(wx.HORIZONTAL)
        self.firstcolour = csel.ColourSelect(self.leftpanel, -1, "First Colour",
                                             self.tree.GetFirstGradientColour())
        self.secondcolour = csel.ColourSelect(self.leftpanel, -1, "Second Colour",
                                              self.tree.GetSecondGradientColour())
        self.firstcolour.Bind(csel.EVT_COLOURSELECT, self.OnFirstColour)
        self.secondcolour.Bind(csel.EVT_COLOURSELECT, self.OnSecondColour)
        sizerb3.Add(self.firstcolour, 0, wx.TOP|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizerb3.Add(self.secondcolour, 0, wx.LEFT|wx.TOP|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizerb.Add(self.checkgradient, 0, wx.ALL, 3)
        sizerb.Add(sizerb1, 0)
        sizerb.Add(sizerb2, 0)
        sizerb.Add(sizerb3, 0, wx.ALIGN_CENTER)

        self.checkvista = wx.CheckBox(self.leftpanel, -1, "Windows Vista Theme")
        self.checkvista.Bind(wx.EVT_CHECKBOX, self.OnVista)

        themessizer.Add(sizera, 0, wx.EXPAND)
        themessizer.Add(sizerb, 0, wx.EXPAND)
        themessizer.Add((0, 5))
        themessizer.Add(self.checkvista, 0, wx.EXPAND|wx.ALL, 3)

        mainsizer.Add(stylesizer, 0, wx.EXPAND|wx.ALL, 5)
        mainsizer.Add(columnssizer, 0, wx.EXPAND|wx.ALL, 5)
        mainsizer.Add(colourssizer, 0, wx.EXPAND|wx.ALL, 5)
        mainsizer.Add(themessizer, 0, wx.EXPAND|wx.ALL, 5)
        mainsizer.Add(eventssizer, 0, wx.EXPAND|wx.ALL, 5)

        self.leftpanel.SetSizer(mainsizer)
        mainsizer.Layout()

        radiobackground.SetValue(1)
        self.checknormal.SetValue(1)
        self.radiohorizontal.Enable(False)
        self.radiovertical.Enable(False)
        self.firstcolour.Enable(False)
        self.secondcolour.Enable(False)
        self.imagebutton.Enable(False)


    def OnRecreateTree(self, event):

        panelparent = self.tree.GetParent()
        panelsizer = panelparent.GetSizer()
        panelparent.Freeze()

        panelsizer.Detach(self.tree)
        self.tree.Destroy()
        del self.tree
        self.tree = HyperTreeList(panelparent, -1, log=self.log)
        panelsizer.Add(self.tree, 1, wx.EXPAND)
        panelsizer.Layout()

        for col in range(self.tree.GetColumnCount()):
            colColour = self.columnBackgroundColours[col]
            self.ColourColumnItems(colColour, col)

        panelparent.Thaw()


    def OnCheckStyle(self, event):

        self.tree.ChangeStyle(self.treestyles)
        event.Skip()


    def OnCheckEvent(self, event):

        obj = event.GetEventObject()
        self.tree.BindEvents(obj)

        event.Skip()


    def OnButtonConnection(self, event):

        pen = self.tree.GetConnectionPen()
        dlg = PenDialog(self, -1, oldpen=pen, pentype=0)

        dlg.ShowModal()
        dlg.Destroy()


    def OnColumnFont(self, event):

        font = self.tree.GetColumn(0).GetFont()

        data = wx.FontData()
        data.SetInitialFont(font)
        dlg = wx.FontDialog(self, data)

        if dlg.ShowModal() == wx.ID_OK:
            data = dlg.GetFontData()
            font = data.GetChosenFont()
            for i in range(0, self.tree.GetMainWindow().GetColumnCount()):
                self.tree.SetColumnFont(i, font)

        dlg.Destroy()
        event.Skip()


    def OnColumnColour(self, event):

        col1 = event.GetValue()
        for i in range(0, self.tree.GetMainWindow().GetColumnCount()):
            self.tree.SetColumnColour(i, col1)

        event.Skip()

    def OnColumnChoiceChanged(self, event):

        selectedColumn = self.columnchoice.GetCurrentSelection()

        if  selectedColumn in range(3):
            colour = self.columnBackgroundColours[selectedColumn]
            self.columnbackgroundcolour.SetValue(colour)

    def ColourColumnItems(self, colour, col):

        def ColourItems(item,colour,col):

            next = item

            while next != None:

                self.tree.SetItemBackgroundColour(next, colour, col)

                cookie=0
                child, cockie = self.tree.GetNextChild(next, cookie)

                while child != None:
                    ColourItems(child, colour, col)
                    child, cookie = self.tree.GetNextChild(next, cookie)

                next = self.tree.GetNextSibling(next)

        root = self.tree.GetRootItem()
        ColourItems(root, colour, col)

    def OnColumnBackgroundColour(self, event):

        columnBackgroundColour = event.GetValue()
        selectedColumn = self.columnchoice.GetCurrentSelection()
        self.columnBackgroundColours[selectedColumn] = columnBackgroundColour
        
        self.ColourColumnItems(columnBackgroundColour, selectedColumn)

        event.Skip()

    def OnColumnAlignment(self, event):

        selection = event.GetSelection()
        if selection == 0:
            alignment = wx.ALIGN_LEFT
        elif selection == 1:
            alignment = wx.ALIGN_CENTER
        else:
            alignment = wx.ALIGN_RIGHT

        for i in range(1, self.tree.GetMainWindow().GetColumnCount()):
            self.tree.SetColumnAlignment(i, alignment)

        event.Skip()


    def OnColumnImages(self, event):

        for i in range(self.tree.GetMainWindow().GetColumnCount()):
            randimage = random.randint(1, len(ArtIDs))
            img = (event.IsChecked() and [randimage] or [-1])[0]
            self.tree.SetColumnImage(i, img)

        event.Skip()

    def OnCustomRenderer(self, event):

        if event.IsChecked():
            renderer = HyperTreeHeaderRenderer(self)
            renderer.color = wx.Colour(211,227,247)
            renderer.text_color = wx.Colour(30,30,30)
            self.tree.SetHeaderCustomRenderer(renderer)
        else:
            renderer = None
            self.tree.SetHeaderCustomRenderer(renderer)

        self.Refresh()

        event.Skip()



    def OnColumnWidth(self, event):

        selection = event.GetSelection()
        if selection == 0:    # wx.LIST_AUTOSIZE_USEHEADER
            choice = wx.LIST_AUTOSIZE_USEHEADER
        else:
            choice = wx.LIST_AUTOSIZE

        for i in range(self.tree.GetMainWindow().GetColumnCount()):
            self.tree.SetColumnWidth(i, choice)

        event.Skip()


    def SetConnectionPen(self, pen):

        self.tree.SetConnectionPen(pen)


    def OnButtonBorder(self, event):

        pen = self.tree.GetBorderPen()
        dlg = PenDialog(self, -1, oldpen=pen, pentype=1)

        dlg.ShowModal()
        dlg.Destroy()


    def SetBorderPen(self, pen):

        self.tree.SetBorderPen(pen)


    def OnButtonTree(self, event):

        dlg = TreeDialog(self, -1, oldicons=self.oldicons)
        dlg.ShowModal()
        dlg.Destroy()


    def OnButtonCheckRadio(self, event):

        dlg = CheckDialog(self, -1)
        dlg.ShowModal()
        dlg.Destroy()


    def SetTreeButtons(self, selection):

        bitmap_plus = opj(bitmapDir + "plus" + str(selection+1) + ".ico")
        bitmap_minus = opj(bitmapDir + "minus" + str(selection+1) + ".ico")

        bitmap = wx.Bitmap(bitmap_plus, wx.BITMAP_TYPE_ICO)
        width = bitmap.GetWidth()

        il = wx.ImageList(width, width)

        il.Add(wx.Bitmap(bitmap_plus, wx.BITMAP_TYPE_ICO))
        il.Add(wx.Bitmap(bitmap_plus, wx.BITMAP_TYPE_ICO))
        il.Add(wx.Bitmap(bitmap_minus, wx.BITMAP_TYPE_ICO))
        il.Add(wx.Bitmap(bitmap_minus, wx.BITMAP_TYPE_ICO))

        self.il = il
        self.tree.SetButtonsImageList(il)


    def SetCheckRadio(self, selection):

        if selection == 0:
            self.tree.SetImageListCheck(13, 13)
        else:
            bitmap_check = opj(bitmapDir + "aquachecked.ico")
            bitmap_uncheck = opj(bitmapDir + "aquanotchecked.ico")
            bitmap_flag = opj(bitmapDir + "aquaflagged.ico")
            bitmap_unflag = opj(bitmapDir + "aquanotflagged.ico")

            il = wx.ImageList(16, 16)

            il.Add(wx.Bitmap(bitmap_check, wx.BITMAP_TYPE_ICO))
            il.Add(wx.Bitmap(bitmap_uncheck, wx.BITMAP_TYPE_ICO))
            il.Add(wx.Bitmap(bitmap_uncheck, wx.BITMAP_TYPE_ICO)) # TODO: we need an "undetermined check" icon...
            il.Add(wx.Bitmap(bitmap_flag, wx.BITMAP_TYPE_ICO))
            il.Add(wx.Bitmap(bitmap_unflag, wx.BITMAP_TYPE_ICO))
            self.tree.SetImageListCheck(16, 16, il)


    def OnBackgroundImage(self, event):

        if hasattr(self, "backgroundimage"):
            self.tree.SetBackgroundImage(self.backgroundimage)

        self.backbutton.Enable(False)
        self.imagebutton.Enable(True)

        event.Skip()


    def OnChooseImage(self, event):

        wildcard = "JPEG Files (*.jpg)|*.jpg|"     \
                   "Bitmap Files (*.bmp)|*.bmp|" \
                   "PNG Files (*.png)|*.png|"    \
                   "Icon Files (*.ico)|*.ico|"        \
                   "GIF Files (*.gif)|*.gif|"        \
                   "All files (*.*)|*.*"

        dlg = wx.FileDialog(self, "Choose An Image File", ".", "", wildcard, wx.FD_OPEN)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
        else:
            dlg.Destroy()
            return

        dlg.Destroy()
        bitmap = wx.Bitmap(path, wx.BITMAP_TYPE_ANY)
        self.tree.SetBackgroundImage(bitmap)
        self.backgroundimage = bitmap

        event.Skip()


    def OnBackgroundColour(self, event):

        self.imagebutton.Enable(False)
        self.backbutton.Enable(True)
        self.tree.SetBackgroundImage(None)

        event.Skip()


    def OnChooseBackground(self, event):

        col1 = event.GetValue()
        self.tree.SetBackgroundColour(col1)
        event.Skip()


    def OnCheckNormal(self, event):

        self.radiohorizontal.Enable(False)
        self.radiovertical.Enable(False)
        self.firstcolour.Enable(False)
        self.secondcolour.Enable(False)
        self.focus.Enable(True)
        self.unfocus.Enable(True)
        self.checkgradient.SetValue(0)
        self.checkvista.SetValue(0)
        self.tree.EnableSelectionGradient(False)
        self.tree.EnableSelectionVista(False)
        event.Skip()


    def OnFocusColour(self, event):

        col1 = event.GetValue()
        self.tree.SetHilightFocusColour(col1)
        event.Skip()


    def OnNonFocusColour(self, event):

        col1 = event.GetValue()
        self.tree.SetHilightNonFocusColour(col1)
        event.Skip()


    def OnCheckGradient(self, event):

        self.radiohorizontal.Enable(True)
        self.radiovertical.Enable(True)
        self.firstcolour.Enable(True)
        self.secondcolour.Enable(True)
        self.checknormal.SetValue(0)
        self.checkvista.SetValue(0)
        self.focus.Enable(False)
        self.unfocus.Enable(False)
        self.tree.SetGradientStyle(self.radiovertical.GetValue())
        self.tree.EnableSelectionVista(False)
        self.tree.EnableSelectionGradient(True)

        event.Skip()


    def OnHorizontal(self, event):

        self.tree.SetGradientStyle(self.radiovertical.GetValue())
        event.Skip()


    def OnVertical(self, event):

        self.tree.SetGradientStyle(self.radiovertical.GetValue())
        event.Skip()


    def OnFirstColour(self, event):

        col1 = event.GetValue()
        self.tree.SetFirstGradientColour(wx.Colour(col1[0], col1[1], col1[2]))
        event.Skip()


    def OnSecondColour(self, event):

        col1 = event.GetValue()
        self.tree.SetSecondGradientColour(wx.Colour(col1[0], col1[1], col1[2]))
        event.Skip()


    def OnVista(self, event):

        self.radiohorizontal.Enable(False)
        self.radiovertical.Enable(False)
        self.firstcolour.Enable(False)
        self.secondcolour.Enable(False)
        self.checknormal.SetValue(0)
        self.checkgradient.SetValue(0)
        self.focus.Enable(False)
        self.unfocus.Enable(False)
        self.tree.EnableSelectionGradient(False)
        self.tree.EnableSelectionVista(True)

        event.Skip()


#---------------------------------------------------------------------------
# HyperTreeList Implementation
#---------------------------------------------------------------------------
class HyperTreeList(HTL.HyperTreeList):

    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition,
                 size=wx.DefaultSize,
                 style=wx.SUNKEN_BORDER,
                 agwStyle=wx.TR_HAS_BUTTONS | wx.TR_HAS_VARIABLE_ROW_HEIGHT,
                 log=None):

        HTL.HyperTreeList.__init__(self, parent, id, pos, size, style, agwStyle)

        alldata = dir(HTL)

        treestyles = []
        events = []
        for data in alldata:
            if data.startswith("TR_"):
                treestyles.append(data)
            elif data.startswith("EVT_"):
                events.append(data)

        events = events + [i for i in dir(wx) if i.startswith("EVT_TREE_")]
        for evt in ["EVT_TREE_GET_INFO", "EVT_TREE_SET_INFO", "EVT_TREE_ITEM_MIDDLE_CLICK",
                    "EVT_TREE_STATE_IMAGE_CLICK"]:
            events.remove(evt)

        treestyles = treestyles + [i for i in dir(wx) if i.startswith("TR_")]
        treeset = {}
        treestyles = [treeset.setdefault(e,e) for e in treestyles if e not in treeset]

        treestyles.sort()

        self.events = events
        self.styles = treestyles
        self.item = None

        il = wx.ImageList(16, 16)

        for items in ArtIDs[1:-1]:
            bmp = wx.ArtProvider.GetBitmap(eval(items), wx.ART_TOOLBAR, (16, 16))
            il.Add(bmp)

        smileidx = il.Add(images.Smiles.GetBitmap())
        numicons = il.GetImageCount()

        self.AssignImageList(il)
        self.count = 0
        self.log = log

        # NOTE:  For some reason tree items have to have a data object in
        #        order to be sorted.  Since our compare just uses the labels
        #        we don't need any real data, so we'll just use None below for
        #        the item data.

        # create some columns
        self.AddColumn("Main column")
        self.AddColumn("Column 1")
        self.AddColumn("Column 2")
        self.SetMainColumn(0) # the one with the tree in it...
        self.SetColumnWidth(0, 175)

        self.root = self.AddRoot("The Root Item")

        if not(self.GetAGWWindowStyleFlag() & wx.TR_HIDE_ROOT):
            self.SetPyData(self.root, None)
            self.SetItemImage(self.root, 24, which=wx.TreeItemIcon_Normal)
            self.SetItemImage(self.root, 13, which=wx.TreeItemIcon_Expanded)
            self.SetItemText(self.root, "col 1 root", 1)
            self.SetItemText(self.root, "col 2 root", 2)

        textctrl = wx.TextCtrl(self.GetMainWindow(), -1, "I Am A Simple\nMultiline wx.TexCtrl", style=wx.TE_MULTILINE)
        self.gauge = wx.Gauge(self.GetMainWindow(), -1, 50, style=wx.GA_HORIZONTAL|wx.GA_SMOOTH)
        self.gauge.SetValue(0)
        combobox = wx.ComboBox(self.GetMainWindow(), -1, choices=["That", "Was", "A", "Nice", "Holiday!"], style=wx.CB_READONLY|wx.CB_DROPDOWN)
        button1 = wx.Button(self.GetMainWindow(), -1, "wxPython")
        button1.SetSize(button1.GetBestSize())
        button2 = wx.Button(self.GetMainWindow(), -1, "Rules!")
        button2.SetSize(button2.GetBestSize())
        listctrl = ListCtrl.TestListCtrlPanel(self.GetMainWindow(), self.log)
        listctrl.SetSize((500, 200))

        textctrl.Bind(wx.EVT_CHAR, self.OnTextCtrl)
        combobox.Bind(wx.EVT_COMBOBOX, self.OnComboBox)

        for x in range(15):
            txt = "Item %d" % x
            if x == 1:
                child = self.AppendItem(self.root, txt + "\nHello World\nHappy wxPython-ing!")
                self.SetItemBold(child, True)
            else:
                child = self.AppendItem(self.root, txt)

            self.SetPyData(child, None)
            self.SetItemText(child, txt + " (c1)", 1)
            self.SetItemText(child, txt + " (c2)", 2)
            self.SetItemImage(child, 24, which=wx.TreeItemIcon_Normal)
            self.SetItemImage(child, 13, which=wx.TreeItemIcon_Expanded)

            for y in range(5):
                txt = "item %d-%s" % (x, chr(ord("a")+y))
                if y == 0 and x == 1:
                    last = self.AppendItem(child, txt, ct_type=2, wnd=self.gauge)
                elif y == 1 and x == 2:
                    last = self.AppendItem(child, txt, ct_type=1, wnd=textctrl)
                    if random.randint(0, 3) == 1:
                        self.SetItem3State(last, True)
                elif 2 < y < 4:
                    last = self.AppendItem(child, txt)
                elif y == 4 and x == 1:
                    last = self.AppendItem(child, txt, wnd=combobox)
                else:
                    last = self.AppendItem(child, txt, ct_type=2)

                self.SetPyData(last, None)
                self.SetItemText(last, txt + " (c1)", 1)
                self.SetItemText(last, txt + " (c2)", 2)
                self.SetItemImage(last, 24, which=wx.TreeItemIcon_Normal)
                self.SetItemImage(last, 13, which=wx.TreeItemIcon_Expanded)

                if y == 3 and x == 0:
                    self.SetItemWindow(last, button1, 1)
                    self.SetItemWindow(last, button2, 2)

                for z in range(5):
                    txt = "item %d-%s-%d" % (x, chr(ord("a")+y), z)
                    if z > 2:
                        item = self.AppendItem(last, txt, ct_type=1)
                        if random.randint(0, 3) == 1:
                            self.SetItem3State(item, True)
                    elif 0 < z <= 2:
                        item = self.AppendItem(last, txt, ct_type=2)
                    elif z == 0:
                        item = self.AppendItem(last, txt)

                    self.SetPyData(item, None)

                    if x == 0 and y == 0 and z == 0:
                        self.SetItemText(item, "", 1)
                        self.SetItemText(item, txt + " (c2)", 2)
                        self.SetItemWindow(item, self.CreateTreeCtrl(), 1)
                    elif x == 0 and y == 0 and z == 1:
                        self.SetItemText(item, txt + " (c1)", 1)
                        self.SetItemText(item, "", 2)
                        self.SetItemWindow(item, listctrl, 2)
                    else:
                        self.SetItemText(item, txt + " (c1)", 1)
                        self.SetItemText(item, txt + " (c2)", 2)
                        if z == 0:
                            self.SetItemHyperText(item, True)

                    self.SetItemImage(item, 28, which=wx.TreeItemIcon_Normal)
                    self.SetItemImage(item, numicons-1, which=wx.TreeItemIcon_Selected)

        self.GetMainWindow().Bind(wx.EVT_LEFT_DCLICK, self.OnLeftDClick)
        self.Bind(wx.EVT_IDLE, self.OnIdle)

        self.eventdict = {'EVT_TREE_BEGIN_DRAG': self.OnBeginDrag, 'EVT_TREE_BEGIN_LABEL_EDIT': self.OnBeginEdit,
                          'EVT_TREE_BEGIN_RDRAG': self.OnBeginRDrag, 'EVT_TREE_DELETE_ITEM': self.OnDeleteItem,
                          'EVT_TREE_END_DRAG': self.OnEndDrag, 'EVT_TREE_END_LABEL_EDIT': self.OnEndEdit,
                          'EVT_TREE_ITEM_ACTIVATED': self.OnActivate, 'EVT_TREE_ITEM_CHECKED': self.OnItemCheck,
                          'EVT_TREE_ITEM_CHECKING': self.OnItemChecking, 'EVT_TREE_ITEM_COLLAPSED': self.OnItemCollapsed,
                          'EVT_TREE_ITEM_COLLAPSING': self.OnItemCollapsing, 'EVT_TREE_ITEM_EXPANDED': self.OnItemExpanded,
                          'EVT_TREE_ITEM_EXPANDING': self.OnItemExpanding, 'EVT_TREE_ITEM_GETTOOLTIP': self.OnToolTip,
                          'EVT_TREE_ITEM_MENU': self.OnItemMenu, 'EVT_TREE_ITEM_RIGHT_CLICK': self.OnRightDown,
                          'EVT_TREE_KEY_DOWN': self.OnKey, 'EVT_TREE_SEL_CHANGED': self.OnSelChanged,
                          'EVT_TREE_SEL_CHANGING': self.OnSelChanging, "EVT_TREE_ITEM_HYPERLINK": self.OnHyperLink}

        mainframe = wx.GetTopLevelParent(self)

        if not hasattr(mainframe, "leftpanel"):
            self.Bind(wx.EVT_TREE_ITEM_EXPANDED, self.OnItemExpanded)
            self.Bind(wx.EVT_TREE_ITEM_COLLAPSED, self.OnItemCollapsed)
            self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnSelChanged)
            self.Bind(wx.EVT_TREE_SEL_CHANGING, self.OnSelChanging)
            self.GetMainWindow().Bind(wx.EVT_RIGHT_DOWN, self.OnRightDown)
            self.GetMainWindow().Bind(wx.EVT_RIGHT_UP, self.OnRightUp)
        else:
            for combos in mainframe.treeevents:
                self.BindEvents(combos)

        if hasattr(mainframe, "leftpanel"):
            self.ChangeStyle(mainframe.treestyles)

        if not(self.GetAGWWindowStyleFlag() & wx.TR_HIDE_ROOT):
            self.SelectItem(self.root)
            self.Expand(self.root)


    def CreateTreeCtrl(self):

        tree = wx.TreeCtrl(self.GetMainWindow(), -1, wx.Point(0, 0), wx.Size(160, 200),
                           wx.TR_DEFAULT_STYLE)

        items = []

        imglist = wx.ImageList(16, 16, True, 2)
        imglist.Add(wx.ArtProvider.GetBitmap(wx.ART_FOLDER, wx.ART_OTHER, wx.Size(16,16)))
        imglist.Add(wx.ArtProvider.GetBitmap(wx.ART_NORMAL_FILE, wx.ART_OTHER, wx.Size(16,16)))
        tree.AssignImageList(imglist)

        root = tree.AddRoot("HyperTreeList :-D", image=0)

        items.append(tree.AppendItem(root, "Item 1", 0))
        items.append(tree.AppendItem(root, "Item 2", 0))
        items.append(tree.AppendItem(root, "Item 3", 0))
        items.append(tree.AppendItem(root, "Item 4", 0))
        items.append(tree.AppendItem(root, "Item 5", 0))

        for ii in range(len(items)):

            id = items[ii]
            tree.AppendItem(id, "Subitem 1", 1)
            tree.AppendItem(id, "Subitem 2", 1)
            tree.AppendItem(id, "Subitem 3", 1)
            tree.AppendItem(id, "Subitem 4", 1)
            tree.AppendItem(id, "Subitem 5", 1)

        tree.Expand(root)
        tree.Expand(items[1])
        tree.Expand(items[3])
        tree.SelectItem(root)

        return tree


    def BindEvents(self, choice, recreate=False):

        value = choice.GetValue()
        text = choice.GetLabel()

        evt = "wx." + text
        binder = self.eventdict[text]

        if value == 1:
            if evt == "wx.EVT_TREE_BEGIN_RDRAG":
                self.GetMainWindow().Bind(wx.EVT_RIGHT_DOWN, None)
                self.GetMainWindow().Bind(wx.EVT_RIGHT_UP, None)
            try:
                self.Bind(eval(evt), binder)
            except:
                self.Bind(eval("HTL." + text), binder)
        else:
            try:
                self.Bind(eval(evt), None)
            except:
                self.Bind(eval("HTL." + text), None)

            if evt == "wx.EVT_TREE_BEGIN_RDRAG":
                self.GetMainWindow().Bind(wx.EVT_RIGHT_DOWN, self.OnRightDown)
                self.GetMainWindow().Bind(wx.EVT_RIGHT_UP, self.OnRightUp)


    def ChangeStyle(self, combos):

        style = 0
        for combo in combos:
            if combo.GetValue() == 1:
                if combo.GetLabel() == "TR_VIRTUAL":
                    style = style | HTL.TR_VIRTUAL
                else:
                    try:
                        style = style | eval("wx." + combo.GetLabel())
                    except:
                        style = style | eval("HTL." + combo.GetLabel())

        if self.GetAGWWindowStyleFlag() != style:
            self.SetAGWWindowStyleFlag(style)


    def OnCompareItems(self, item1, item2):

        t1 = self.GetItemText(item1)
        t2 = self.GetItemText(item2)

        self.log.write('compare: ' + t1 + ' <> ' + t2 + "\n")

        if t1 < t2:
            return -1
        if t1 == t2:
            return 0

        return 1


    def OnIdle(self, event):

        if self.gauge:

            try:
                if self.gauge.IsEnabled() and self.gauge.IsShown():
                    self.count = self.count + 1

                    if self.count >= 50:
                        self.count = 0

                    self.gauge.SetValue(self.count)

            except:

                self.gauge = None

        event.Skip()


    def OnRightDown(self, event):

        pt = event.GetPosition()
        item, flags, column = self.HitTest(pt)

        if item:
            self.item = item
            self.log.write("OnRightClick: %s, %s, %s\n" % (self.GetItemText(item), type(item), item.__class__))
            self.SelectItem(item)


    def OnRightUp(self, event):

        item = self.item

        if not item:
            event.Skip()
            return

        if not self.IsItemEnabled(item):
            event.Skip()
            return

        # Item Text Appearance
        ishtml = self.IsItemHyperText(item)
        back = self.GetItemBackgroundColour(item)
        fore = self.GetItemTextColour(item)
        isbold = self.IsBold(item)
        font = self.GetItemFont(item)

        # Icons On Item
        normal = self.GetItemImage(item, wx.TreeItemIcon_Normal)
        selected = self.GetItemImage(item, wx.TreeItemIcon_Selected)
        expanded = self.GetItemImage(item, wx.TreeItemIcon_Expanded)
        selexp = self.GetItemImage(item, wx.TreeItemIcon_SelectedExpanded)

        # Enabling/Disabling Windows Associated To An Item
        haswin = self.GetItemWindow(item)

        # Enabling/Disabling Items
        enabled = self.IsItemEnabled(item)

        # Generic Item's Info
        children = self.GetChildrenCount(item)
        itemtype = self.GetItemType(item)
        text = self.GetItemText(item)
        pydata = self.GetPyData(item)

        self.current = item
        self.itemdict = {"ishtml": ishtml, "back": back, "fore": fore, "isbold": isbold,
                         "font": font, "normal": normal, "selected": selected, "expanded": expanded,
                         "selexp": selexp, "haswin": haswin, "children": children,
                         "itemtype": itemtype, "text": text, "pydata": pydata, "enabled": enabled}

        menu = wx.Menu()

        item2 = menu.Append(wx.ID_ANY, "Modify Item Text Colour")
        menu.AppendSeparator()
        if isbold:
            strs = "Make Item Text Not Bold"
        else:
            strs = "Make Item Text Bold"
        item3 = menu.Append(wx.ID_ANY, strs)
        item4 = menu.Append(wx.ID_ANY, "Change Item Font")
        item13 = menu.Append(wx.ID_ANY, "Change Item Background Colour")
        menu.AppendSeparator()
        if ishtml:
            strs = "Set Item As Non-Hyperlink"
        else:
            strs = "Set Item As Hyperlink"
        item5 = menu.Append(wx.ID_ANY, strs)
        menu.AppendSeparator()

        item7 = menu.Append(wx.ID_ANY, "Disable Item")

        menu.AppendSeparator()
        item14 = menu.Append(wx.ID_ANY, "Hide Item")
        item15 = menu.Append(wx.ID_ANY, "Unhide All Items")
        menu.AppendSeparator()
        item8 = menu.Append(wx.ID_ANY, "Change Item Icons")
        menu.AppendSeparator()
        item9 = menu.Append(wx.ID_ANY, "Get Other Information For This Item")
        menu.AppendSeparator()

        item10 = menu.Append(wx.ID_ANY, "Delete Item")
        if item == self.GetRootItem():
            item10.Enable(False)
        item11 = menu.Append(wx.ID_ANY, "Prepend An Item")
        item12 = menu.Append(wx.ID_ANY, "Append An Item")

        self.Bind(wx.EVT_MENU, self.OnItemForeground, item2)
        self.Bind(wx.EVT_MENU, self.OnItemBold, item3)
        self.Bind(wx.EVT_MENU, self.OnItemFont, item4)
        self.Bind(wx.EVT_MENU, self.OnItemHyperText, item5)
        self.Bind(wx.EVT_MENU, self.OnDisableItem, item7)
        self.Bind(wx.EVT_MENU, self.OnItemIcons, item8)
        self.Bind(wx.EVT_MENU, self.OnItemInfo, item9)
        self.Bind(wx.EVT_MENU, self.OnItemDelete, item10)
        self.Bind(wx.EVT_MENU, self.OnItemPrepend, item11)
        self.Bind(wx.EVT_MENU, self.OnItemAppend, item12)
        self.Bind(wx.EVT_MENU, self.OnItemBackground, item13)
        self.Bind(wx.EVT_MENU, self.OnHideItem, item14)
        self.Bind(wx.EVT_MENU, self.OnUnhideItems, item15)

        self.PopupMenu(menu)
        menu.Destroy()
        event.Skip()


    def OnItemForeground(self, event):

        colourdata = wx.ColourData()
        colourdata.SetColour(self.itemdict["fore"])
        dlg = wx.ColourDialog(self, colourdata)

        dlg.GetColourData().SetChooseFull(True)

        if dlg.ShowModal() == wx.ID_OK:
            data = dlg.GetColourData()
            col1 = data.GetColour().Get()
            self.SetItemTextColour(self.current, col1)
        dlg.Destroy()
        event.Skip()


    def OnItemBold(self, event):

        self.SetItemBold(self.current, not self.itemdict["isbold"])
        event.Skip()


    def OnItemFont(self, event):

        data = wx.FontData()
        font = self.itemdict["font"]

        if font is None:
            font = wx.SystemSettings_GetFont(wx.SYS_DEFAULT_GUI_FONT)

        data.SetInitialFont(font)

        dlg = wx.FontDialog(self, data)

        if dlg.ShowModal() == wx.ID_OK:
            data = dlg.GetFontData()
            font = data.GetChosenFont()
            self.SetItemFont(self.current, font)

        dlg.Destroy()
        event.Skip()


    def OnItemHyperText(self, event):

        self.SetItemHyperText(self.current, not self.itemdict["ishtml"])
        event.Skip()


    def OnDisableItem(self, event):

        self.EnableItem(self.current, False)
        event.Skip()


    def OnHideItem(self, event):

        self.HideItem(self.current)
        event.Skip()


    def OnUnhideItems(self, event):

        item = self.GetRootItem()
        while item:
            if item.IsHidden():
                self.HideItem(item, False)
            item = self.GetNext(item)
        event.Skip()


    def OnItemIcons(self, event):

        bitmaps = [self.itemdict["normal"], self.itemdict["selected"],
                   self.itemdict["expanded"], self.itemdict["selexp"]]

        wx.BeginBusyCursor()
        dlg = TreeIcons(self, -1, bitmaps=bitmaps)
        wx.EndBusyCursor()
        dlg.ShowModal()
        dlg.Destroy()


    def SetNewIcons(self, bitmaps):

        self.SetItemImage(self.current, bitmaps[0], which=wx.TreeItemIcon_Normal)
        self.SetItemImage(self.current, bitmaps[1], which=wx.TreeItemIcon_Selected)
        self.SetItemImage(self.current, bitmaps[2], which=wx.TreeItemIcon_Expanded)
        self.SetItemImage(self.current, bitmaps[3], which=wx.TreeItemIcon_SelectedExpanded)


    def OnItemInfo(self, event):

        itemtext = self.itemdict["text"]
        numchildren = str(self.itemdict["children"])
        itemtype = self.itemdict["itemtype"]
        pydata = repr(type(self.itemdict["pydata"]))

        if itemtype == 0:
            itemtype = "Normal"
        elif itemtype == 1:
            itemtype = "CheckBox"
        else:
            itemtype = "RadioButton"

        strs = "Information On Selected Item:\n\n" + "Text: " + itemtext + "\n" \
               "Number Of Children: " + numchildren + "\n" \
               "Item Type: " + itemtype + "\n" \
               "Item Data Type: " + pydata + "\n"

        dlg = wx.MessageDialog(self, strs, "HyperTreeListDemo Info", wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()

        event.Skip()


    def OnItemDelete(self, event):

        strs = "Are You Sure You Want To Delete Item " + self.GetItemText(self.current) + "?"
        dlg = wx.MessageDialog(None, strs, 'Deleting Item', wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_QUESTION)

        if dlg.ShowModal() in [wx.ID_NO, wx.ID_CANCEL]:
            dlg.Destroy()
            return

        dlg.Destroy()

        self.DeleteChildren(self.current)
        self.Delete(self.current)
        self.current = None

        event.Skip()


    def OnItemPrepend(self, event):

        dlg = wx.TextEntryDialog(self, "Please Enter The New Item Name", 'Item Naming', 'Python')

        if dlg.ShowModal() == wx.ID_OK:
            newname = dlg.GetValue()
            newitem = self.PrependItem(self.current, newname)
            self.EnsureVisible(newitem)

        dlg.Destroy()
        event.Skip()


    def OnItemAppend(self, event):

        dlg = wx.TextEntryDialog(self, "Please Enter The New Item Name", 'Item Naming', 'Python')

        if dlg.ShowModal() == wx.ID_OK:
            newname = dlg.GetValue()
            newitem = self.AppendItem(self.current, newname)
            self.EnsureVisible(newitem)

        dlg.Destroy()
        event.Skip()


    def OnItemBackground(self, event):

        colourdata = wx.ColourData()
        colourdata.SetColour(self.itemdict["back"])
        dlg = wx.ColourDialog(self, colourdata)

        dlg.GetColourData().SetChooseFull(True)

        if dlg.ShowModal() == wx.ID_OK:
            data = dlg.GetColourData()
            col1 = data.GetColour().Get()
            self.SetItemBackgroundColour(self.current, col1)

        dlg.Destroy()
        event.Skip()


    def OnBeginEdit(self, event):

        self.log.write("OnBeginEdit\n")
        # show how to prevent edit...
        item = event.GetItem()
        if item and self.GetItemText(item) == "The Root Item":
            wx.Bell()
            self.log.write("You can't edit this one...\n")

            # Lets just see what's visible of its children
            cookie = 0
            root = event.GetItem()
            (child, cookie) = self.GetFirstChild(root)

            while child:
                self.log.write("Child [%s] visible = %d\n" % (self.GetItemText(child), self.IsVisible(child)))
                (child, cookie) = self.GetNextChild(root, cookie)

            event.Veto()


    def OnEndEdit(self, event):

        self.log.write("OnEndEdit: %s %s\n" %(event.IsEditCancelled(), event.GetLabel()))
        # show how to reject edit, we'll not allow any digits
        for x in event.GetLabel():
            if x in string.digits:
                self.log.write("You can't enter digits...\n")
                event.Veto()
                return


    def OnLeftDClick(self, event):

        pt = event.GetPosition()
        item, flags, column = self.HitTest(pt)
        if item and (flags & wx.TREE_HITTEST_ONITEMLABEL):
            if self.GetAGWWindowStyleFlag() & wx.TR_EDIT_LABELS:
                self.log.write("OnLeftDClick: %s (manually starting label edit)\n"% self.GetItemText(item))
                self.EditLabel(item)
            else:
                self.log.write("OnLeftDClick: Cannot Start Manual Editing, Missing Style TR_EDIT_LABELS\n")

        event.Skip()


    def OnItemExpanded(self, event):

        item = event.GetItem()
        if item:
            self.log.write("OnItemExpanded: %s\n" % self.GetItemText(item))


    def OnItemExpanding(self, event):

        item = event.GetItem()
        if item:
            self.log.write("OnItemExpanding: %s\n" % self.GetItemText(item))

        event.Skip()


    def OnItemCollapsed(self, event):

        item = event.GetItem()
        if item:
            self.log.write("OnItemCollapsed: %s" % self.GetItemText(item))


    def OnItemCollapsing(self, event):

        item = event.GetItem()
        if item:
            self.log.write("OnItemCollapsing: %s\n" % self.GetItemText(item))

        event.Skip()


    def OnSelChanged(self, event):

        self.item = event.GetItem()
        if self.item:
            self.log.write("OnSelChanged: %s" % self.GetItemText(self.item))
            if wx.Platform == '__WXMSW__':
                self.log.write(", BoundingRect: %s\n" % self.GetBoundingRect(self.item, True))
            else:
                self.log.write("\n")

        event.Skip()


    def OnSelChanging(self, event):

        item = event.GetItem()
        olditem = event.GetOldItem()

        if item:
            if not olditem:
                olditemtext = "None"
            else:
                olditemtext = self.GetItemText(olditem)
            self.log.write("OnSelChanging: From %s To %s\n" %(olditemtext, self.GetItemText(item)))

        event.Skip()


    def OnBeginDrag(self, event):

        self.item = event.GetItem()
        if self.item:
            self.log.write("Beginning Drag...\n")

            event.Allow()


    def OnBeginRDrag(self, event):

        self.item = event.GetItem()
        if self.item:
            self.log.write("Beginning Right Drag...\n")

            event.Allow()


    def OnEndDrag(self, event):

        self.item = event.GetItem()
        if self.item:
            self.log.write("Ending Drag!\n")

        event.Skip()


    def OnDeleteItem(self, event):

        item = event.GetItem()

        if not item:
            return

        self.log.write("Deleting Item: %s\n" % self.GetItemText(item))
        event.Skip()


    def OnItemCheck(self, event):

        item = event.GetItem()
        self.log.write("Item " + self.GetItemText(item) + " Has Been Cheched!\n")
        event.Skip()


    def OnItemChecking(self, event):

        item = event.GetItem()
        self.log.write("Item " + self.GetItemText(item) + " Is Being Checked...\n")
        event.Skip()


    def OnToolTip(self, event):

        item = event.GetItem()
        if item:
            event.SetToolTip(wx.ToolTip(self.GetItemText(item)))


    def OnItemMenu(self, event):

        item = event.GetItem()
        if item:
            self.log.write("OnItemMenu: %s\n" % self.GetItemText(item))

        event.Skip()


    def OnKey(self, event):

        keycode = event.GetKeyCode()
        keyname = keyMap.get(keycode, None)

        if keycode == wx.WXK_BACK:
            self.log.write("OnKeyDown: HAHAHAHA! I Vetoed Your Backspace! HAHAHAHA\n")
            return

        if keyname is None:
            if "unicode" in wx.PlatformInfo:
                keycode = event.GetUnicodeKey()
                if keycode <= 127:
                    keycode = event.GetKeyCode()
                keyname = "\"" + unichr(event.GetUnicodeKey()) + "\""
                if keycode < 27:
                    keyname = "Ctrl-%s" % chr(ord('A') + keycode-1)

            elif keycode < 256:
                if keycode == 0:
                    keyname = "NUL"
                elif keycode < 27:
                    keyname = "Ctrl-%s" % chr(ord('A') + keycode-1)
                else:
                    keyname = "\"%s\"" % chr(keycode)
            else:
                keyname = "unknown (%s)" % keycode

        self.log.write("OnKeyDown: You Pressed '" + keyname + "'\n")

        event.Skip()


    def OnActivate(self, event):

        if self.item:
            self.log.write("OnActivate: %s\n" % self.GetItemText(self.item))

        event.Skip()


    def OnHyperLink(self, event):

        item = event.GetItem()
        if item:
            self.log.write("OnHyperLink: %s\n" % self.GetItemText(self.item))


    def OnTextCtrl(self, event):

        keycode = event.GetKeyCode()
        char = chr(keycode) if keycode < 256 else ''
        self.log.write("EDITING THE TEXTCTRL: You Wrote '" + char + \
                       "' (KeyCode = " + str(keycode) + ")\n")
        event.Skip()


    def OnComboBox(self, event):

        selection = event.GetEventObject().GetValue()
        self.log.write("CHOICE FROM COMBOBOX: You Chose '" + selection + "'\n")
        event.Skip()


#---------------------------------------------------------------------------


class TestPanel(wx.Panel):
    def __init__(self, parent, log):
        self.log = log
        wx.Panel.__init__(self, parent, -1)

        b = wx.Button(self, -1, " Test HyperTreeList ", (50,50))
        self.Bind(wx.EVT_BUTTON, self.OnButton, b)


    def OnButton(self, evt):
        self.win = HyperTreeListDemo(self, self.log)
        self.win.Show(True)

#----------------------------------------------------------------------

def runTest(frame, nb, log):
    win = TestPanel(nb, log)
    return win

#----------------------------------------------------------------------


overview = HTL.__doc__


if __name__ == '__main__':
    import sys,os
    import run
    run.main(['', os.path.basename(sys.argv[0])] + sys.argv[1:])

