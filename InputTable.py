'''
Created on Jan 4, 2020

@author: justin
'''

import wx
from .ComboBox import ComboBox


class InputTable(wx.Panel):
    """
    Creates a simple for data entry 
    with labels
    """

    def __init__(self, parent, controldefn):
        wx.Panel.__init__(self, parent, -1, style=wx.WANTS_CHARS)

        self.PanelSizer = wx.FlexGridSizer(0, 2, 0, 0)
        self.SetSizer(self.PanelSizer)
        self.PanelSizer.SetFlexibleDirection(wx.BOTH)
        self.PanelSizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.ControlDefn = controldefn
        self.ControlDict = {}
        self.CreateControls()
        self.Layout()
        self.PanelSizer.Fit(self)

    def CreateControls(self):
        """
        uses the data in self.Controls to create the interface
        self.ControlDefn will have the following format:
        [{"Title": String, "Type": "String|Int|Float|Choice", "Params": Something}]
        """

        for control in self.ControlDefn:
            label = wx.StaticText(self, wx.ID_ANY, control["Title"], wx.DefaultPosition, wx.DefaultSize, 0)

            if control["Type"] == "String":
                ctrl = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
            elif control["Type"] == "Choice":
                ctrl = ComboBox(self, choices=control.get("Choices", []))
            elif control["Type"] == "Int":
                Min = control["Min"] if "Min" in control else 0
                Max = control["Max"] if "Max" in control else 100
                Default = control["Default"] if "Default" in control else 0
                
                ctrl = wx.SpinCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, Min, Max, Default)

            self.PanelSizer.Add(label, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)
            self.PanelSizer.Add(ctrl, 0, wx.ALL | wx.EXPAND, 5)

            self.ControlDict[control["Title"]] = {"Control": ctrl, "Type": control["Type"]}

    def SetData(self, DataDict):
        """
        Sets the data from the control
        data dict will be of {key:value} form
        
        if the value is a string, performs a strip on the data
        """
        
        for key, value in DataDict.items():
            if key in self.ControlDict:
                if type(value) == str:
                    value = value.strip()
                self.ControlDict[key]["Control"].SetValue(value)


    def GetData(self):
        """
        Gets the data from the control
        """
        ControlData = {key: self.ControlDict[key]["Control"].GetValue() for key in self.ControlDict.keys()}
        return ControlData 
    
    def OnDataButton(self, event):
        print("button clicked")
        
    def GetControl(self, ControlName):
        """
        Returns the control
        """
        return self.ControlDict[ControlName]["Control"]
