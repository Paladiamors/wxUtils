'''
Created on Jan 3, 2020

@author: justin
'''

from .TreeListCtrl import TreeListCtrl, dv
import os


class BrowserCtrl(TreeListCtrl):

    def __init__(self, parent):
        TreeListCtrl.__init__(self, parent)
        self.AppendColumn("Files", width=100, flags=dv.DATAVIEW_COL_RESIZABLE | dv.DATAVIEW_COL_SORTABLE)
        self.AppendColumn("Size", width=100, flags=dv.DATAVIEW_COL_RESIZABLE | dv.DATAVIEW_COL_SORTABLE)
        self.ScannedFolders = set(["/"])
        self.LoadLevel("/", 1)
        
        self.Bind(dv.EVT_TREELIST_ITEM_EXPANDED, self.OnExpanded)

    def LoadLevel(self, path, depth=0):

        walker = os.walk(path)
        try:
            base, folders, files = walker.__next__()
            folders.sort()
            files.sort()

            for folder in folders:
                self.AddFolder(os.path.join(base, folder))

            for f in files:
                self.AddObject(os.path.join(base, f), colhandler = self.ColHandler)

            if depth:
                for folder in folders:
                    self.LoadLevel(os.path.join(base, folder), depth - 1)
        except StopIteration:
            pass

    def OnExpanded(self, event):
        
        item = event.GetItem()
        path = self.GetItemData(item)
        if path not in self.ScannedFolders:
            self.ScannedFolders.add(path)
            self.LoadLevel(path, 1)
            
    def ColHandler(self, item, **kwargs):
        self.SetItemText(item, 1, "100 KB")