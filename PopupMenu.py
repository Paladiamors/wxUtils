'''
Created on Jan 4, 2020

@author: justin
'''

import wx


class PopupMenu(wx.Menu):

    def __init__(self, Selections):
        """
        Selections = List of dictionaries [{"Title": "Title", "Func": function}]
        we generate a new Id reference for each menu if it does not exist
        and then perform the bind 
        """
        wx.Menu.__init__(self)

        for selection in Selections:
            if "Id" not in selection:
                selection["Id"] = wx.NewIdRef()
            self.Bind(wx.EVT_MENU, selection["Func"], selection["Id"])

            self.Append(selection["Id"], selection["Title"])
